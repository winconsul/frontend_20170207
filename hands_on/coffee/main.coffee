Vue = require 'vue'
http = require 'axios'
baseUrl = "http://localhost:3000"

new Vue
    el: '#main'
    data: () ->
        # 初回データを取得する
        http.get "#{baseUrl}/api/users"
        .then (res) =>
            @users = res.data.users

        data =
            users: []
            message:
                success: ''

        return data

    methods:
        # 更新ボタンをクリックしたときに呼ばれる
        update: (user) ->
            # noが設定されているかどうかで登録/更新を分ける。
            if user.no?
                alert "not implemented!"
                return
            else
                p = http.post "#{baseUrl}/api/users", {
                    name: user.name,
                    birthday: user.birthday
                }
                .then (res) =>
                    @success "登録が成功しました。"
                    return

            # 更新と登録のどちらでも、成功したら全ユーザの情報を更新する
            p.then () =>
                http.get "#{baseUrl}/api/users"
            .then (res) =>
                @users = res.data.users
                return

            return

        # 削除ボタンをクリックしたときに呼ばれる
        delete: (user) ->
            alert "not implemented!"
            return

        # 追加ボタンをクリックしたときに呼ばれる
        newUser: () ->
            # IDを仮採番しておく
            @users.push { id: "-#{new Date().getTime()}" }
            return

        # messageを表示する
        success: (message) ->
            @message.success = message

            setTimeout () =>
                @message.success = ''
            , 5000
