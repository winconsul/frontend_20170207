"use strict";

module.exports = function(req, res) {
    var db = require('./database');

    db.remove({ _id: req.params.id }, function(err, n){
        if (err) {
            res.status(404);
            res.json({
                success: false
            });
        } else {
            res.json({
                success: true
            });
        }
    });
};
