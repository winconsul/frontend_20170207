"use strict";

module.exports = function(req, res) {
    var db = require("./database");
    db.update({ _id: req.params.id }, { $set: {
        name: req.body.name,
        birthday: req.body.birthday
    }}, {}, function(err, n){
        res.json({
            success: true
        });
    });
};
