"use strict";

module.exports = function(req, res) {
    var db = require("./database");

    db.find({}).sort({ no: -1 }).limit(1).exec(function(e, d){
        db.insert([{
            no: (e ? 0 : d[0].no) + 1,
            name: req.body.name,
            birthday: req.body.birthday
        }], function(err, docs) {
            var doc = docs[0];
            res.json({
                id: doc._id,
                no: doc.no,
                name: doc.name,
                birthday: doc.birthday
            });
        });
    });
};
