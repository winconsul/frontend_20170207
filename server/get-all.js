"use strict";

module.exports = function(req, res) {
    var db = require('./database');

    db.find({}).sort({ no: 1 }).exec(function(err, docs){
        var users = [], i, d;

        for (i = 0; i < docs.length; i++) {
            d = docs[i];
            users.push({
                id: d._id,
                no: d.no,
                name: d.name,
                birthday: d.birthday
            });
        }

        res.json({
            users: users
        });
    });
};
