"use strict";

module.exports = function(req, res) {
    var db = require("./database");

    db.remove({}, { multi: true }, function(){
        db.insert([
            { no: 1, name: "明石家 健",   birthday: "2016/04/07" },
            { no: 2, name: "松本 敦",     birthday: "2016/03/13" },
            { no: 3, name: "平沢 遥",     birthday: "2016/02/29" },
            { no: 4, name: "石川 千佳子", birthday: "2016/04/25" },
            { no: 5, name: "落合 さやか", birthday: "2016/03/01" },
            { no: 6, name: "井手 隆博",   birthday: "2016/08/07" },
            { no: 7, name: "乾 育子",     birthday: "2016/06/06" },
        ], function(err, newDocs) {
            res.json({
                success: true
            });
        });
    });
}