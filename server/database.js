'use strict';

var Database = require('nedb');
var db = new Database({
    filename: './users.db',
    autoload: true
});

module.exports = db;
