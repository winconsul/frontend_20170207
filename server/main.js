module.exports = function (port) {

    var express = require('express');
    var app = express();
    var bodyParser = require('body-parser');

    app.set('json spaces', 2);
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    var router = express.Router()

    router.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        next();
    });

    router.get('/init', require('./init'));
    router.get('/users', require('./get-all'));
    router.post('/users', require('./register'));
    router.get('/users/:id', require('./get-one'));
    router.put('/users/:id', require('./update'));
    router.delete('/users/:id', require('./delete'));

    app.use('/api', router);

    // ポートを指定して起動する
    app.listen(port);
}
