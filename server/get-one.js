"use strict";

module.exports = function(req, res) {
    var db = require('./database');

    db.findOne({ _id: req.params.id }, function(err, doc){

        if (err) {
            res.status(404).json({
                success: false
            })
        } else {
            res.json({
                id: doc._id,
                no: doc.no,
                name: doc.name,
                birthday: doc.birthday
            });
        }
    });
};
