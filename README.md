frontend_0207
===============

npm start後
----------------------

* `localhost:3000`でRESTを受けつけるサーバが起動しています。
* `localhost:3474`でhands_on以下にアクセスするサーバが起動しています。
* `localhost:3475`でhands_on\_exampleにアクセスするサーバが起動しています。
* `localhost:9000`でプレゼンを見られます。

api
----------------------

|       URL      | メソッド | 概要                                            |
| :------------- | :------- | :---------------------------------------------- |
| /api/init      | GET      | 現在の情報を削除して、初期データを登録します。  |
| /api/users     | GET      | 全ユーザを取得します。                          |
| /api/users     | POST     | 新規ユーザを登録します。                        |
| /api/users/:id | GET      | idで指定したユーザの情報を取得します。          |
| /api/users/:id | PUT      | idで指定したユーザの情報を更新します。          |
| /api/users/:id | DELETE   | idで指定したユーザの情報を削除します。          |

### GET /api/users

#### レスポンス

```
{
    users : [
        {
            id: '(IDを表す文字列)',
            no: 1,
            name: 'テスト 太郎',
            birthday: '1988-04-02'
        }, {
            id : '(IDを表す文字列)',
            name: 'テスト 次郎',
            birthday: '1990-12-31'
        }
    ]
}
```

### POST /api/users

#### リクエスト

```
{
    name: 'テスト 太郎',
    birthday: '1988-04-02'
}
```

#### レスポンス

```
{
    id: '(IDを表す文字列)',
    no: 1,
    name: 'テスト 太郎',
    birthday: '1988-04-02'
}
```

### GET /api/users/:id

#### リクエスト

/api/users/(IDを表す文字列)

#### レスポンス

```
{
    id: '(IDを表す文字列)',
    no: 1,
    name: 'テスト 太郎',
    birthday: '1988-04-02'
}
```

### PUT /api/users/:id

#### リクエスト

/api/users/(IDを表す文字列)

```
{
    name: 'テスト 太郎',
    birthday: '1988-04-02'
}
```

#### レスポンス

200 OK

```
{
    success: true
}
```

または

400 Bad Request

```
{
    success: false,
    message: 'nameが不足しています'
}
```

### DELETE /api/users/:id

#### リクエスト

/api/users/(IDを表す文字列)

#### レスポンス

200 OK

```
{
    success: true
}
```

または

404 Not Found

```
{
    success: false,
    message: 'ユーザは存在しません'
}
```

ヒント
------------------

* [axios](https://github.com/mzabriskie/axios)
* [vue.js](http://v1-jp.vuejs.org/guide/)
