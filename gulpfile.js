'use strict';

var browserify  = require('browserify');
var gulp        = require('gulp');
var filter      = require('gulp-filter');
var fs          = require('fs-extra');
var notify      = require('gulp-notify');
var rename      = require('gulp-rename');
var watch       = require('gulp-watch');
var source      = require('vinyl-source-stream');
var superstatic = require('superstatic').server;
var path        = require('path');

gulp.task('start-server', function(){
    var hands_on = superstatic({
        port: 3474,
        cwd: "hands_on"
    }).listen(function(){});

    var hands_on_example = superstatic({
        port: 3475,
        cwd: "hands_on_example"
    }).listen(function(){});

    var presentation = superstatic({
        port: 9000,
        cwd: "presentation"
    }).listen(function(){});

    var server = require('./server/main');
    server(3000);
});

function prepare(name, options) {
    var BUILD_TASK = 'build-' + name;
    var WATCH_TASK = 'watch-' + name;

    gulp.task(BUILD_TASK, function() {
        return browserify({
            entries: [ options.src_dir + '/main.coffee' ],
            extensions: ['.coffee'],
            transform: ['coffeeify']
        }).bundle()
          .on('error', function(){
              notify.onError({
                  title: 'compile error',
                  message: '<%= error %>'
              }).apply(this, Array.prototype.slice.call(arguments));
              //this.emit('end');
          })
          .pipe(source('main.js'))
          .pipe(rename('bundle.js'))
          .pipe(gulp.dest(options.dst_dir));
    });

    gulp.task(WATCH_TASK, function(){
        gulp.start(BUILD_TASK);

        watch(options.src_dir + '/**/*', function(){
            gulp.start(BUILD_TASK);
        });
    });
}

prepare('hands_on', {
    src_dir: 'hands_on/coffee',
    dst_dir: 'hands_on'
});

prepare('hands_on_example', {
    src_dir: 'hands_on_example/coffee',
    dst_dir: 'hands_on_example'
});

prepare('presentation', {
    src_dir: 'presentation/coffee',
    dst_dir: 'presentation'
});

gulp.task('prepare-reveal', function(){
    fs.copySync('node_modules/reveal.js/css',    'presentation/css');
    fs.copySync('node_modules/reveal.js/lib',    'presentation/lib');
    fs.copySync('node_modules/reveal.js/plugin', 'presentation/plugin');
});

gulp.task('watch', ['prepare-reveal',
                    'watch-hands_on',
                    'watch-hands_on_example',
                    'watch-presentation',
                    'start-server']);

gulp.task('default', ['watch']);
